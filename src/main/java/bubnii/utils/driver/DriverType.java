package bubnii.utils.driver;

public enum DriverType {
    CHROME, MOZILLA, OPERA;
}
