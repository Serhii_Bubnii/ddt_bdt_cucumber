package bubnii.utils.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.stream.Stream;

public final class CSVUtils {

    private static final String COMMA_SPLIT = ", *";

    public static Iterator<Object[]> parseCSV(String csvPath) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(csvPath))) {
            Stream.Builder<Object[]> builder = Stream.builder();
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_SPLIT);
                builder.add(values);
            }
            return builder.build().iterator();
        }
    }
}
