package bubnii.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AuthorizationPage extends PageObject {

    @FindBy(id = "identifierId")
    private WebElement loginInput;
    @FindBy(xpath = "//*[@id='password']//div[1]//div//div[1]//input")
    private WebElement passwordInput;
    @FindBy(css = "span.RveJvd.snByac")
    private WebElement submitLoginButton;
    @FindBy(id = "passwordNext")
    private WebElement submitPasswordButton;

    public AuthorizationPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void fillUpUserLogin(String userLogin) {
        this.loginInput.clear();
        this.loginInput.sendKeys(userLogin);
    }

    public void submitLogin() {
        this.submitLoginButton.click();
    }

    public void fillUpUserPassword(String userPassword) {
        this.passwordInput.clear();
        this.passwordInput.sendKeys(userPassword);
    }

    public void submitPassword() {
        new WebDriverWait(webDriver, 10)
                .until(ExpectedConditions.elementToBeClickable(
                        By.cssSelector("div#passwordNext[role='button']")));
        submitPasswordButton.click();
    }

}
