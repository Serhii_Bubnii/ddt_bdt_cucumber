package bubnii.bo;

import bubnii.entity.User;
import bubnii.po.AuthorizationPage;
import org.openqa.selenium.WebDriver;

public class AuthorizationBo {

    private AuthorizationPage authorizationPage;

    public AuthorizationBo(WebDriver webDriver) {
        authorizationPage = new AuthorizationPage(webDriver);
    }

    public void login(User user){
        authorizationPage.fillUpUserLogin(user.getEmail());
        authorizationPage.submitLogin();
        authorizationPage.fillUpUserPassword(user.getPassword());
        authorizationPage.submitPassword();
    }
}
