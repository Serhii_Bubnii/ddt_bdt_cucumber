Feature: Send a message and a copy and a hidden copy of the message

  Scenario Outline: User log in successfully
    Given User is on Gmail LogIn page
    When User log in as "<email>", "<password>"
    And Sends Email and copy and hidden copy Email
    Then User verifies whether last sent Email is sent
    And WebDriver is closed

    Examples:
      | email                   | password   |
      | testacount968@gmail.com | tao1234123 |
      | testacount969@gmail.com | tao1234123 |
      | testacount963@gmail.com | tao1234123 |
      | testacount971@gmail.com | tao1234123 |