package bubnii.cucumber.steps;

import bubnii.bo.AuthorizationBo;
import bubnii.bo.GmailBo;
import bubnii.entity.Email;
import bubnii.entity.User;
import bubnii.utils.driver.AbstractDriverManager;
import bubnii.utils.driver.DriverManagerFactory;
import bubnii.utils.driver.DriverType;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

public class GmailStepdefs {

    protected static String USER_EMAIL_RECEIVER = "bubniy1995@gmail.com";
    protected static String USER_EMAIL_CC = "testacount963@gmail.com";
    protected static String USER_EMAIL_BCC = "testacount971@gmail.com";
    private AbstractDriverManager driverManager;
    private WebDriver webDriver;
    private AuthorizationBo authorizationBo;
    private GmailBo gmailBo;

    @Given("^User is on Gmail LogIn page$")
    public void userIsOnGmailLogInPage() {
        driverManager = DriverManagerFactory.getDriverManager(DriverType.CHROME);
        webDriver = driverManager.getWebDriver();
        webDriver.get("https://mail.google.com/mail");
    }

    @When("^User log in as \"([^\"]*)\", \"([^\"]*)\"$")
    public void userLogInAs(String email, String password) {
        authorizationBo.login(new User().setEmail(email).setPassword(password));
    }

    @And("^Sends Email and copy and hidden copy Email$")
    public void sendsEmailAndCopyAndHiddenCopyEmail() {
        gmailBo.sendMessage(new Email()
                .setReceiver(USER_EMAIL_RECEIVER)
                .setReceiverCopy(USER_EMAIL_CC)
                .setReceiverHiddenCopy(USER_EMAIL_BCC)
                .setSubject("EPAM")
                .setMessage("ky-ky"));
    }

    @Then("^User verifies whether last sent Email is sent$")
    public void userVerifiesWhetherLastSentEmailIsSent() {
        throw new UnsupportedOperationException();
    }

    @And("^WebDriver is closed$")
    public void webdriverIsClosed() {
        driverManager.quitWebDriver();
    }
}
