package bubnii.seleniumhm;

import bubnii.utils.csv.CSVUtils;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.Iterator;
import java.util.ResourceBundle;

public class UserDataProvide {

    @DataProvider(parallel = true)
    public Iterator<Object[]> users() throws IOException {
       return CSVUtils.parseCSV(ResourceBundle.getBundle("config_web_driver").getString("CSV_PATH"));
    }
}
