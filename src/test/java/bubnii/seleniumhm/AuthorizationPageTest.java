package bubnii.seleniumhm;

import bubnii.bo.AuthorizationBo;
import bubnii.bo.GmailBo;
import bubnii.entity.Email;
import bubnii.entity.User;
import bubnii.utils.driver.AbstractDriverManager;
import bubnii.utils.driver.DriverManagerFactory;
import bubnii.utils.driver.DriverType;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AuthorizationPageTest {

    protected static String USER_EMAIL_RECEIVER = "bubniy1995@gmail.com";
    protected static String USER_EMAIL_CC = "testacount963@gmail.com";
    protected static String USER_EMAIL_BCC = "testacount971@gmail.com";
    protected static WebDriver webDriver;
    protected static AbstractDriverManager driverManager;
    private AuthorizationBo authorizationBo;
    private GmailBo gmailBo;

    @BeforeClass(alwaysRun = true)
    public static void setUp() {
        driverManager = DriverManagerFactory.getDriverManager(DriverType.CHROME);
        webDriver = driverManager.getWebDriver();

    }

    @Test(dataProvider = "users", dataProviderClass = UserDataProvide.class)
    public void testSendEmail(String userEmail, String userPassword) {
        webDriver.get("https://mail.google.com/mail");
        authorizationBo = new AuthorizationBo(webDriver);
        gmailBo = new GmailBo(webDriver);
        authorizationBo.login(new User()
                .setEmail(userEmail)
                .setPassword(userPassword));
        gmailBo.sendMessage(new Email()
                .setReceiver(USER_EMAIL_RECEIVER)
                .setReceiverCopy(USER_EMAIL_CC)
                .setReceiverHiddenCopy(USER_EMAIL_BCC)
                .setSubject("EPAM")
                .setMessage("ky-ky"));
    }

    @AfterClass
    public static void tearDown() {
        driverManager.quitWebDriver();
    }

}
